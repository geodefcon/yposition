from django.db import models
from django.contrib import admin


class Domain(models.Model):
    name = models.CharField(max_length=100, blank=True, unique=True)
    keywords = models.CharField(max_length=100, blank=True)
    position = models.CharField(max_length=100, blank=True)
    
    def __unicode__(self):
        return unicode(self.name)


class DomainAdmin(admin.ModelAdmin):
    list_display = ['name', 'keywords']
        


    